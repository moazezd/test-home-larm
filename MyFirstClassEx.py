class Employee:
    def __init__(self, first, last, pay):
        self.name = first
        self.surname = last
        self.salary = pay
        self.email = first + last + '@hotmail.com'

    def fullname(self):
        return 'from method:{} {}'.format(self.name, self.surname)


emp_1 = Employee('davood', 'Moazez', 5000)

print('From class instants:')
print('{} {}'.format(emp_1.name, emp_1.surname))
print(Employee.fullname(emp_1))
print('from instants1:', emp_1.fullname())
