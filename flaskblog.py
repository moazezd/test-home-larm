#added render-templates function to fetch 
#html part from templates files
from flask import Flask , render_template
app = Flask(__name__)
#List with two dictionery to pass to oure template 
post1 = [
    {
        'author':'Cory schafer',
        'title':'Blog post 1',
        'content': 'First post content',
        'data_posted': 'April 20,2020'
    },
    {
        'author':'Jane Doe',
        'title':'Blog post 2',
        'content': 'Second post content',
        'data_posted': 'April 21,2020'  
    }
]
@app.route('/')
@app.route('/home')
#fetch from template
#add post
def home():
    return render_template('home.html',post=post1)
@app.route('/about')
def about():
    #pass title from templates
    return render_template('about.html',title='about')
if __name__ == "__main__":
    app.run(debug=True)
